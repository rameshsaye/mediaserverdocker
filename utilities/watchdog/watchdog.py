#!/usr/bin/python3
import os
import logging
import requests
import time, sys
import subprocess
import socket

from subprocess import Popen, PIPE
from logging.handlers import RotatingFileHandler
CRITICAL_FLAG=False
PROCESS_STATUS=False
PROCESS_ONGOING=False
PROCESS_STOP=False


serverSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
serverSock.bind(("", PLC_PORT_NO))    
serverSock.settimeout(5.0)

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(levelname)-8s %(message)s',
                    datefmt='%a, %d %b %Y %H:%M:%S',
                    filename=LOG_FILENAME,
                    filemode='a+')
logger = logging.getLogger('my_logger')
handler = RotatingFileHandler(LOG_FILENAME, maxBytes=50*1024*1024, backupCount=10)
logger.addHandler(handler)
logging.debug('Media WatchDog Started !!!')
