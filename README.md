# README



> The Following is the guide to Setup Media server docker for streaming camera with mpeg-dash, hls and webrtc.

Take AWS, Amazon, Blade or NUC

## Install Ubuntu 18.04 LTS and Setup
*	Step 1 : Create bootable USB	
	*	[GUIDE TO CREATE BOOTABLE USB for WINDOWS](http://www.linuxandubuntu.com/home/make-ubuntu-bootable-usb-in-windows-10)
	*	[GUIDE TO CREATE BOOTABLE USB for LINUX](https://tutorials.ubuntu.com/tutorial/tutorial-create-a-usb-stick-on-ubuntu#0)
*	step 2 : Ubuntu 18.04 Installation 
	*	[Installation GUIDE](https://tutorials.ubuntu.com/tutorial/tutorial-install-ubuntu-desktop#0)
		*	(Select option  "install third party software for graphics and wifi hardware")
		*	Time-zone -- kolkata
		*	**name-`media` , computer's name-`media-doc` , username-`media` , password-`media123` (ALL lower CASE letters)**
		*	Enable auto-login option
*   Step 3 : Turn-Off display 
	*	[PREVENT DISPLAY SLEEPING AND TURN OFF LOCKSCREEN](https://websiteforstudents.com/disable-turn-off-ubuntu-18-04-lts-beta-lock-screen/)
	*	disable showing notification on lockscreen.
*   Step 4 : Set Static IP for Ethernet
	*	[SET STATIC IP FOR ETHERNET](https://liimages/logonuxconfig.org/how-to-configure-static-ip-address-on-ubuntu-18-04-bionic-beaver-linux)
	*	Station ip : `192.168.1.2`
	*	Netmask : `255.255.255.0`
*	Step 5 : Connect to WIFI `for internet`(**THIS IS OPTIONAL**)
*	Step 6 : Setup wake on Power
	*   `FOR MOOTECH BOX PC`
    *	Go to BIOS ( Press ESC after power-on )
	*	Goto **CHIPSET**>**PCH-IO Configuration**>**State afet G3** -- make it "**S0 state**"
README.md	*   For intel NUC , go to BIOS > POWER > State after power failure > Last State and disable SECURE boot
    *	Save Changes and Exit from BIOS



## Docker Setup


### Run the following commands 
*	`sudo apt update`
*	`sudo apt upgrade`images/logo
*	`sudo apt install -y vim git make curl apt-transport-https ca-certificates software-properties-common openssh-server sendip tcpdump python3-pip


sudo apt-get remove docker docker-engine docker.io
Step 3: Install Docker
To install Docker on Ubuntu, in the terminal window enter the command:

sudo apt install docker.io
Step 4: Start and Automate Docker
The Docker service needs to be setup to run at startup. To do so, type in each command followed by enter:

sudo systemctl start docker
sudo systemctl enable docker


keep the following cammands handy



### Clone Docker Repo
*	`git clone https://ArvindUmrao07@bitbucket.org/arvind_umrao/mediaserverdocker.git`
*   `git submodule init`
*   `git submodule update`
*   `cd mediaserverdocker/`
*	`make install` **(System will restart after this step)**,**(Execute this step only for the Initial Setup)** ,**(Do Not Run this on a Dev System, or personal laptops)**
*	`make build-docker-image` **(Instead of this , you may follow docker image build steps at the bottom of this readme)**
*	`cd ~/docker`
*	`./start-container.sh` **(Run this for Starting Container)**
*	`./start-container.sh /docker/src/build-all.sh` **(Run this for building the code - You do not need to do it unless you are developer)**

docker ps
docker attach  psid
docker exec -it  065524c4b9af /bin/bash

### Help

*	docker starts up **automatically** when the PC wakesup or aws restart.
*	You need to attach to docker container for development or testing by running command `docker attach media` + 	`TAB` button  (docker name is mediaCCU-dAtETiMe).
*	detach from docker -- `ctrl+P` +  `ctrl+Q`, if you want to attach back , user `docker attach media` + `TAB`
*	detach from screen -- `ctrl+a` +  `d`, if you want to attach back to screen , use `screen -r `




## Also possible with Docker Image

Please avoid `make build-docker-image`. It takes around 20 mins to build docker. Better copy from Link.

Please Use the following link to get the latest docker image:
> Version Controling will be done using the Datetime of upload

[ Link to Download Docker Image](https://xxxx.com)



Open terminal in the folder where the file is downloaded
Load image using : `docker load -i <docker-image-filename>`
RUN: `docker tag  $(docker images | grep -A 1 "IMAGE ID" | awk '{getline; print $3}') docker:latest`




![GitHub Logo](/mediaserver.png)



