
IMAGE_TAG=docker

ROOT_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))

.PHONY: build-docker-image
build-docker-image:
	find src/ -name "*core.*docker*" -exec rm -f {} \;
	docker build $(ROOT_DIR) -t $(IMAGE_TAG)

.PHONY: install
install:
	wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
	echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | sudo tee /etc/apt/sources.list.d/google-chrome.list
	sudo cp -v mediadoc-container.service /etc/systemd/system/
	sudo cp -v /home/media/docker/runscreenrc.sh.desktop /home/media/.config/autostart/
	sudo chmod 664 /etc/systemd/system/mediadoc-container.service
	sudo systemctl enable mediadoc-container.service
	sudo apt-get install google-chrome-stable
	sudo apt install net-tools curl docker.io screen
	sudo apt update
	sudo apt upgrade
	ssh-keygen
	sudo usermod -aG docker $(USER)
	echo -e "Restarting the system.............."
	sleep 10s
	sudo reboot
