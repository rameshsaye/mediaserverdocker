#!/bin/bash
#
set -ex
#

systemctl stop  signalling
systemctl start  signalling


systemctl stop  mediaserver
systemctl start  mediaserver


systemctl stop  webrtc
systemctl start  webrtc