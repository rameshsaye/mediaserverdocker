# MediaServer with Webrtc Dockerfile

# Base system
FROM ubuntu:18.04
RUN sed -i 's/:\/\/archive.ubuntu.com/:\/\/in.archive.ubuntu.com/g' /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y locales apt-utils curl
RUN apt-get dist-upgrade -y

# Set the locale
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get install -y build-essential cmake gcc g++ \
     git pkg-config \
     apt-transport-https \
     software-properties-common

RUN apt-get install -y unzip


# autoconf
RUN apt-get install -y libtool automake autoconf autogen


RUN apt-get install libssl-dev -y


RUN apt install libsdl2-dev libsdl2-2.0-0 -y
RUN apt install libfdk-aac-dev -y

RUN apt-get update


# External Sources
RUN apt-get install -y apt-transport-https

RUN apt-get install -y  nasm


ADD  webrtc_android /workspace/webrtc_android/
ADD  setup/key   /var/tmp/key/

COPY src/mediaserver.service /etc/systemd/system/
COPY src/webrtc.service /etc/systemd/system/
COPY src/signalling.service /etc/systemd/system/


WORKDIR /workspace



RUN git clone   https://github.com/mirror/x264.git 

RUN cd x264 && \
    ./configure   --disable-opencl --enable-static && \
    make -j8 && \
    make install


RUN git clone -b release/3.3 https://github.com/FFmpeg/FFmpeg ffmpeg
RUN cd ffmpeg && \
./configure --pkg-config-flags="--static" --libdir=/usr/local/lib --disable-shared --enable-static --enable-gpl --enable-pthreads --enable-nonfree  --enable-libfdk-aac    --enable-libx264 --enable-filters --enable-runtime-cpudetect --disable-lzma && \
make -j8 && \
    make install


RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get update
RUN  apt-get install nodejs -y 



RUN apt-get install -y strace
RUN apt-get install -y screen rfkill
RUN apt-get update
#RUN apt-get install -y nodejs npm



RUN git clone -b master https://github.com/akumrao/live.git live
RUN cd live && \
./genMakefiles linux-64bit && \
 make -j8 && \
 make  install




RUN git clone -b provigil https://github.com/akumrao/mediaserver.git mediaserver
RUN cd mediaserver/src/broadcast/main && \
make -j8  


RUN cd mediaserver/src/broadcast/signalserver/codelab && \
npm install

RUN cd mediaserver/src/rtsp/main && \
make -j8 

#COPY src/config/hosts.docker /etc/hosts.docker



RUN apt-get update
RUN apt-get install -y net-tools sendip sudo vim gdb
RUN apt-get install -y expect
RUN apt-get install -y netcat iputils-ping

RUN apt-get install -y tcpdump

RUN apt-get install -y vim

RUN apt-get install -y ctags


RUN apt-get install -y python3-pip



# Install sshpass
RUN apt-get install -y sshpass

# Install rsync
RUN apt-get install -y rsync

RUN apt-get install -y systemd

# Cleanup
RUN apt-get clean all
RUN ldconfig -v

# ENV SRC=/src
# WORKDIR ${SRC}
# ADD src ${SRC}
# VOLUME src
WORKDIR /docker
